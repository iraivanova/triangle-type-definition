const defineKindOfTriangle = (sideA, sideB, sideC) => {
  if (
    sideA >= sideB + sideC ||
    sideC >= sideA + sideB ||
    sideB >= sideA + sideC
  )
    return 'not exist';

  const sideASquare = Math.pow(sideA, 2);
  const sideBSquare = Math.pow(sideB, 2);
  const sideCSquare = Math.pow(sideC, 2);
  const sumSquareTwoSides = sideBSquare + sideASquare;

  if (sideCSquare === sumSquareTwoSides) {
    return 'rectangular';
  } else if (sideCSquare < sumSquareTwoSides) {
    return 'acute-angled';
  }
  return 'obtuse';
};

module.exports = defineKindOfTriangle;